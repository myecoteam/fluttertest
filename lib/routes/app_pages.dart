import 'package:get/get.dart';
import 'package:test1/views/1.root/rootview_binding.dart';
import 'package:test1/views/1.root/rootview_page.dart';
import 'app_routes.dart';

class AppPages {
  static var list =[
    GetPage(
      name: AppRoutes.ROOT,
      page: () => RootViewPage(),
      binding: RootViewBinding(),
    ),
  ];
}
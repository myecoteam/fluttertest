import 'package:get/get.dart';
import 'package:test1/views/1.root/rootview_controller.dart';

class RootViewBinding extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<RootViewController>(() => RootViewController());
  }

}

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'rootview_controller.dart';

class RootViewPage extends GetView<RootViewController> {
  @override
  Widget build(BuildContext context) {

    List _list = ['IOS MOA', 'IOS YES24', 'IOS OPMS', 'IOS PDF', 'ANDROID MOA', 'ANDROID YES24', 'ANDROID OPMS', 'ANDROID PDF'];

    // TODO: implement build
    return GetBuilder<RootViewController>(builder: (controller) {

      return Scaffold(
        appBar: AppBar(
          title: Text('전자책테스트'),
        ),
        body: GridView.builder(
            itemCount: _list.length,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio:  1,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10
            ),
            itemBuilder: (context, index) {
              return Container(
                color: getColor(index),
                child: TextButton(
                    onPressed: () {

                    },
                    child: Text(_list.elementAt(index).toString(), style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),)
                ),
              );
            },
        ),
      );

    });
  }

  getColor(int index) {
    switch(index) {

      case 0 :
        return Colors.deepOrange;
      case 1 :
        return Colors.deepOrange.shade300;
      case 2 :
        return Colors.deepOrange.shade200;
      case 3 :
        return Colors.deepOrange.shade50;
      case 4 :
        return Colors.blueAccent.shade700;
      case 5 :
        return Colors.blueAccent.shade400;
      case 6 :
        return Colors.blueAccent;
      case 7 :
        return Colors.blueAccent.shade100;
    }

  }

}